#[allow(unused_macros)]
macro_rules! execute {
    ($commands:expr, $interpreter:expr) => {
    use qr::parser;
    let mut stream = $commands.as_bytes();
    parser::parse(&mut stream, $interpreter, false);
    };
}

#[allow(unused_macros)]
macro_rules! assert_stack {
    ($command:expr, $expected:expr, $dtype:expr, $dtype_conv_function:ident, $should_end:expr) => {{
    use qr::interpreter::QRInterpreter;

    let mut interpreter = QRInterpreter::new();

    execute!($command, &mut interpreter);

    for out_val in $expected.into_iter() {
    let pushed_value = interpreter.stack.pop_one();
    assert_eq!(pushed_value.get_data_type(), $dtype);
    assert_eq!(&pushed_value.$dtype_conv_function(), out_val);
    }
    if $should_end {
        assert_stack_end!(interpreter);
    }
    interpreter
    }};
}

#[allow(unused_macros)]
macro_rules! execute_command {
    ($command:expr) => {
    use qr::interpreter::QRInterpreter;

    let mut interpreter = QRInterpreter::new();

    execute!($command, &mut interpreter);
    };
}

#[allow(unused_macros)]
macro_rules! assert_stack_values {
    ($interpreter:expr, $expected:expr, $dtype:expr, $dtype_conv_function:ident) => {
    for out_val in $expected.into_iter() {
        let pushed_value = $interpreter.stack.pop_one();
        assert_eq!(pushed_value.get_data_type(), $dtype);
        assert_eq!(&pushed_value.$dtype_conv_function(), out_val);
    }
    };
}

#[allow(unused_macros)]
macro_rules! assert_stack_top_null {
    ($interpreter:expr) => {{
    use qr::memory::data::DataType;
    let value = $interpreter.stack.pop_one();
    assert_eq!(value.get_data_type(), DataType::Null);
    }};
}

#[allow(unused_macros)]
macro_rules! assert_stack_end {
    ($interpreter:expr) => {
        use std::panic;
        let result = panic::catch_unwind(panic::AssertUnwindSafe(|| $interpreter.stack.get(1)));
        if !result.is_err() {
            panic!("Pushed too many values onto the stack");
        }
    };
}

