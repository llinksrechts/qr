mod push {
    use qr::interpreter::QRInterpreter;
    use qr::memory::data::DataType;

    #[test]
    fn null() {
        let command = "q q q";
        let mut interpreter = QRInterpreter::new();

        execute!(command, &mut interpreter);

        assert_stack_top_null!(interpreter);
        assert_stack_end!(interpreter);
    }

    #[test]
    fn pos_num() {
        let command = "q q qq q qqqq";
        let expected = [3];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn neg_num() {
        let command = "q q qq qq qqqq";
        let expected = [-4];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn lower_chr() {
        let command = "q q qqq q qqq";
        let expected = ['c'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn upper_chr() {
        let command = "q q qqq qq qq";
        let expected = ['B'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn special_chr() {
        let command = "q q qqq qqq qqq";
        let expected = ['!'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn numeral_chr() {
        let command = "q q qqq qqqq qqq";
        let expected = ['2'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn ascii_chr() {
        let command = "q q qqq qqqqq qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq";
        let expected = ['%'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn array() {
        let command = "q q qqqq qq q qqqq qq qqq qqqq qqqqqq";
        let expected = [1, 2, 3, 5];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn str() {
        let command = "q q qqqqq qq qqqqqqqq q qqqqqqqqq qqqqqq";
        let expected = ['H', 'i'];
        let mut interpreter = assert_stack!(command, expected, DataType::Char, as_chr, false);

        assert_stack_top_null!(interpreter);
        assert_stack_end!(interpreter);
    }

    #[test]
    fn ns() {
        let command = "q q qqqqqq qqq qq q qqq";
        let expected = [&vec![2u64, 1, 3]];
        assert_stack!(command, expected, DataType::Namespace, as_ns, true);
    }

    #[test]
    fn dtype() {
        let command = "q q qqqqqqq q       - null\
                            q q qqqqqqq qq      - number\
                            q q qqqqqqq qqq     - char\
                            q q qqqqqqq qqqq    - boolean\
                            q q qqqqqqq qqqqq   - float\
                            q q qqqqqqq qqqqqq  - namespace\
                            q q qqqqqqq qqqqqqq - error";
        let expected = [DataType::Error, DataType::Namespace, DataType::Float,
                                    DataType::Bool, DataType::Char, DataType::Num,
                                    DataType::Null];
        assert_stack!(command, expected, DataType::Datatype, cmp_data_type, true);
    }
}

mod pop {
    use qr::memory::data::DataType;

    #[test]
    fn one() {
        let command = "q q qq q q - push 0\
                            q q q      - push null\
                            q qq q     - pop one";
        let expected = [0];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn many() {
        let command = "q q qq q qq - push 1\
                            q q qq q q  - push 0\
                            q q q       - push null\
                            q qq qq qq  - pop 2";
        let expected = [1];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn string() {
        let command = "q q qq q q  - push 0\
                            q q q       - push null\
                            q q qq q qq - push 1\
                            q q qqq q q - push a\
                            q qq qqq    - pop str";
        let expected = [0];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn stack() {
        let command = "q q qq q q    - push 0\
                            q q qq q qq   - push 1\
                            q q qq q qqq  - push 2\
                            q q qq q qqqq - push 3\
                            q q qq q qqq  - push 2\
                            q qq qqqq     - pop stack";
        let expected = [1, 0];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }
}

mod get {
    use qr::memory::data::DataType;

    #[test]
    fn single() {
        let command = "q q qq q q    - push 0\
                            q q qq q qq   - push 1\
                            q q qq q qqq  - push 2\
                            q qqq         - get";
        let expected = [0, 1, 0];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn string() {
        let command = "q q qq q q    - push 0\
                            q q q         - push null\
                            q q qq q qq   - push 1\
                            q q qq q qq   - push 1\
                            q q qq q q    - push 0\
                            q q qq q qqq  - push 2\
                            q qqqq        - get str";
        let expected = [];
        let mut interpreter = assert_stack!(command, expected, DataType::Num, as_num, false);
        assert_stack_values!(interpreter, [1, 1], DataType::Num, as_num);
        assert_stack_top_null!(interpreter);
        assert_stack_values!(interpreter, [0, 1, 1], DataType::Num, as_num);
        assert_stack_top_null!(interpreter);
        assert_stack_values!(interpreter, [0], DataType::Num, as_num);
    }
}

mod m0ve {
    use qr::memory::data::DataType;

    #[test]
    fn single() {
        let command = "q q qq q q    - push 0\
                            q q qq q qq   - push 1\
                            q q qq q qqq  - push 2\
                            q qqqqq       - move";
        let expected = [0, 1];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn string() {
        let command = "q q qq q q    - push 0\
                            q q q         - push null\
                            q q qq q qq   - push 1\
                            q q qq q qq   - push 1\
                            q q qq q q    - push 0\
                            q q qq q qqq  - push 2\
                            q qqqqqq      - move str";
        let expected = [];
        let mut interpreter = assert_stack!(command, expected, DataType::Num, as_num, false);
        assert_stack_values!(interpreter, [1, 1], DataType::Num, as_num);
        assert_stack_top_null!(interpreter);
        assert_stack_values!(interpreter, [0, 0], DataType::Num, as_num);
    }
}
