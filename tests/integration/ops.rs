use qr::memory::data::DataType;

#[test]
fn add() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qq q          - add";
    let expected = [5];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn sub() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qq qq         - sub";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn multiply() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qq qqq        - multiply";
    let expected = [6];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn div() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qq qqqq       - div";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn m0d() {
    let command = "q q qq q qqqqqq - push 5\
                        q q qq q qqqq   - push 3\
                        qq qqqqq        - mod";
    let expected = [2];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn and() {
    let command = "q q qq q qqqqqqq - push 6\
                        q q qq q qqqq    - push 3\
                        qq qqqqqq        - bit and";
    let expected = [2];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn or() {
    let command = "q q qq q qqqqqqq - push 6\
                        q q qq q qqqq    - push 3\
                        qq qqqqqqq       - bit or";
    let expected = [7];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn xor() {
    let command = "q q qq q qqqqqqq - push 6\
                        q q qq q qqqq    - push 3\
                        qq qqqqqqqq      - bit xor";
    let expected = [5];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn not() {
    let command = "q q qq q qqqqqqq - push 6\
                        qq qqqqqqqqq     - bit not";
    let expected = [249];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn pow() {
    let command = "q q qq q qqqqqqq - push 6\
                        q q qq q qqqq    - push 3\
                        qq qqqqqqqqqq    - pow";
    let expected = [216];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn get_dtype() {
    let command = "q q qq q qqqqqqq - push 6\
                        qq qqqqqqqqqqq   - get dtype";
    let expected = [DataType::Num];
    let mut interpreter = assert_stack!(command, expected, DataType::Datatype, cmp_data_type, false);
    assert_stack_values!(interpreter, [6], DataType::Num, as_num);
    assert_stack_end!(interpreter);
}

mod cast {
    use qr::memory::data::DataType;

    #[test]
    fn to_num() {
        let command = "q q qqq q qqq - push c\
                            qqqqqqq qq    - cast to num";
        let expected = [99];
        assert_stack!(command, expected, DataType::Num, as_num, true);
    }

    #[test]
    fn to_char() {
        let command = "q q qq q qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq - push 0x30\
                            qqqqqqq qqq - cast to char";
        let expected = ['0'];
        assert_stack!(command, expected, DataType::Char, as_chr, true);
    }

    #[test]
    fn to_bool() {
        let command = "q q qq q q   - push 0\
                            qqqqqqq qqqq - cast to bool\
                            q q qq q qq  - push 1
                            qqqqqqq qqqq - cast to bool";
        let expected = [true, false];
        assert_stack!(command, expected, DataType::Bool, as_bool, true);
    }

    #[test]
    fn to_float() {
        let command = "q q qq q qqqqq - push 4\
                            qqqqqqq qqqqq  - cast to float";
        let expected = [4.0];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }
}
