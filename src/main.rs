use std::env;

extern crate qr;

use qr::util;
use qr::parser;
use qr::interpreter::{self, Interpreter};

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut fname = None;
    let mut interactive = false;
    if args.len() > 1 {
        fname = Some(&args[1]);
    }
    if fname.is_none() {
        interactive = true;
    }
    let ref mut interpreter = interpreter::QRInterpreter::new();
    if !interactive {
        interpreter.add_parameters(Vec::from(&args[2..]));
    }
    if interactive {
        parser::parse(&mut util::get_stdin_reader(), interpreter, interactive);
    } else {
        parser::parse(&mut util::get_file_reader(fname.unwrap()), interpreter, interactive);
    }
}
