use std::fs::File;
use std::io::{BufReader, stdin, Stdin};

/// Gets a file reader for a given file name
///
/// # Arguments
/// * `fname` - the file path
///
/// # Returns
/// The file reader
pub fn get_file_reader(fname: &str) -> BufReader<File> {
    let f = match File::open(fname) {
        Ok(f) => f,
        Err(e) => { let desc; { use std::error::Error; desc = e.description(); } error!(3199, desc) },
    };

    BufReader::new(f)
}

/// Gets a reader for stdin
///
/// # Returns
/// The stdin reader
pub fn get_stdin_reader() -> BufReader<Stdin> {
    BufReader::new(stdin())
}

/// Performs the extended euclidean algorithm
///
/// # Arguments
/// * `num` - the first number
/// * `rem` - the second number
///
/// # Returns
/// The greatest common divisor
///
/// # Example
/// ```
/// let n1 = 2 * 5 * 7 * 11;    // 770
/// let n2 = 3 * 5 * 11 * 17;   // 2805
/// let gcd = 11;
///
/// use qr::util::xgcd;
/// let result = xgcd(n1, n2);
/// assert_eq!(result, gcd);
/// ```
pub fn xgcd(mut num: i64, mut rem: i64) -> i64 {
    let (mut x0, mut x1, mut y0, mut y1) = (1i64, 0i64, 0i64, 1i64);
    while num != 0 {
        let q = rem / num;
        let r = rem;
        rem = num;
        num = r % num;
        let x = x0;
        x0 = x1;
        x1 = x - q * x1;
        let y = y0;
        y0 = y1;
        y1 = y - q * y1;
    }
    y0
}

/// Performs a modular power (a^n mod p)
///
/// # Arguments
/// * `base` - the base `a`
/// * `power` - the power `n`
/// * `rem` - the modulo `p`
///
/// # Returns
/// The result of the calculation
///
/// # Example
/// ```
/// let base = 2;
/// let power = 137;
/// let modulo = 13;
/// let expected = 6;
///
/// use qr::util::mod_pow;
/// let result = mod_pow(base, power, modulo);
/// assert_eq!(expected, result);
/// ```
pub fn mod_pow(mut base: i64, mut power: i64, rem: i64) -> i64 {
    if power < 0 {
        base = xgcd(base, rem);
        power = -power;
    }
    if power == 0 {
        return 1;
    }
    let mut y = 1i64;
    while power > 1 {
        if power % 2 == 1 {
            y = (base * y) % rem;
            base = (base * base) % rem;
            power = (power - 1) / 2;
        } else {
            base = (base * base) % rem;
            power /= 2;
        }
    }
    (base * y) % rem
}