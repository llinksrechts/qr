macro_rules! num_as_q {
    ($num:expr) => {(0..$num).map(|_| "q").collect::<String>()};
}

macro_rules! fmt_data_vec {
    ($vec:expr) => {$vec.iter().map(|d| { d.to_string() }).collect::<Vec<_>>().join("")};
}

macro_rules! str_to_data_vec {
    ($str:expr) => {{
        use memory::data::{Data, Char};
        $str.into_iter().map(|chr| Box::new(Char::from(chr as char)) as Box<Data>).rev().collect::<Vec<_>>()
    }};
}

macro_rules! data_vec_to_byte_array {
    ($vec:expr) => {$vec.iter().map(|d| { d.as_num() as u8 }).collect::<Vec<_>>()};
}

macro_rules! byte_array_to_data_vec {
    ($arr:expr, $len:expr) => {{
        use memory::data::{Data, Num};
        $arr.into_iter().take($len).map(|b| Box::new(Num::from(*b as i64)) as Box<Data>).collect::<Vec<_>>().into_iter().rev().collect::<Vec<_>>()
    }};
}
