use std::fmt;
use std::clone::Clone;
use std::cmp::Ordering;

/// All possible data types
#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub enum DataType {
    /// Null value
    Null,
    /// Any integer
    Num,
    /// Floating point number
    Float,
    /// Single character
    Char,
    /// Boolean
    Bool,
    /// Function namespace
    Namespace,
    /// Information about a data type
    Datatype,
    /// Error (error code + message)
    Error,
}

macro_rules! no_math {
    () => {error!(2001)};
}

/// A single piece of data
pub trait Data: fmt::Display + DataClone {
    /// Sets or adds a value to the data
    ///
    /// # Arguments
    /// * `data` - the value to add
    ///
    /// # Returns
    /// Whether the piece of data is complete
    fn add_data(&mut self, data: u64) -> bool;
    /// Indicates if the piece of data has all necessary information
    ///
    /// # Returns
    /// A boolean indicating whether the piece of data is complete
    fn is_complete(&self) -> bool;
    /// Checks whether the piece of data is NULL
    ///
    /// # Returns
    /// True if it is null, false otherwise
    fn is_null(&self) -> bool {
        self.get_data_type() == DataType::Null
    }

    /// Performs an addition with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform addition with
    ///
    /// # Returns
    /// A boxed version of the result
    fn add(&self, other: &Box<Data>) -> Box<Data> {
        if self.get_data_type() == DataType::Float || other.get_data_type() == DataType::Float {
            let result = self.as_float() + other.as_float();
            Box::new(Float(result, true))
        } else {
            if self.get_data_type() == DataType::Error || other.get_data_type() == DataType::Error {
                no_math!();
            }
            let result = self.as_num() + other.as_num();
            Box::new(Num(result, true))
        }
    }
    /// Performs a subtraction with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform subtraction with
    ///
    /// # Returns
    /// A boxed version of the result
    fn sub(&self, other: &Box<Data>) -> Box<Data> {
        if self.get_data_type() == DataType::Float || other.get_data_type() == DataType::Float {
            let result = self.as_float() - other.as_float();
            Box::new(Float(result, true))
        } else {
            if self.get_data_type() == DataType::Error || other.get_data_type() == DataType::Error {
                no_math!();
            }
            let result = self.as_num() - other.as_num();
            Box::new(Num(result, true))
        }
    }
    /// Performs a multiplication with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform multiplication with
    ///
    /// # Returns
    /// A boxed version of the result
    fn mul(&self, other: &Box<Data>) -> Box<Data> {
        if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2002, self.get_data_type(), other.get_data_type());
        }
        if self.get_data_type() == DataType::Float || other.get_data_type() == DataType::Float {
            let result = self.as_float() * other.as_float();
            Box::new(Float(result, true))
        } else {
            let result = self.as_num() * other.as_num();
            Box::new(Num(result, true))
        }
    }
    /// Performs a division with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform division with
    ///
    /// # Returns
    /// A boxed version of the result
    fn div(&self, other: &Box<Data>) -> Box<Data> {
        if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2003, self.get_data_type(), other.get_data_type());
        }
        if self.get_data_type() == DataType::Float || other.get_data_type() == DataType::Float {
            let result = self.as_float() / other.as_float();
            Box::new(Float(result, true))
        } else {
            let result = self.as_num() / other.as_num();
            Box::new(Num(result, true))
        }
    }
    /// Performs a modulo with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform modulo with
    ///
    /// # Returns
    /// A boxed version of the result
    fn rem(&self, other: &Box<Data>) -> Box<Data> {
        if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2003, self.get_data_type(), other.get_data_type());
        }
        if self.get_data_type() == DataType::Float || other.get_data_type() == DataType::Float {
            let result = self.as_float() % other.as_float();
            Box::new(Float(result, true))
        } else {
            let result = self.as_num() % other.as_num();
            Box::new(Num(result, true))
        }
    }
    /// Performs a bitwise and with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform and with
    ///
    /// # Returns
    /// A boxed version of the result
    fn and(&self, other: &Box<Data>) -> Box<Data> {
        if self.get_data_type() == DataType::Bool && other.get_data_type() == DataType::Bool {
            return Box::new(Bool::from(self.as_bool() & other.as_bool()));
        } else if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2004, self.get_data_type(), other.get_data_type());
        } else if self.get_data_type() == DataType::Float {
            error!(2004);
        }
        let result = self.as_num() & other.as_num();
        Box::new(Num(result, true))
    }
    /// Performs a bitwise or with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform or with
    ///
    /// # Returns
    /// A boxed version of the result
    fn or(&self, other: &Box<Data>) -> Box<Data> {
        if self.get_data_type() == DataType::Bool && other.get_data_type() == DataType::Bool {
            return Box::new(Bool::from(self.as_bool() | other.as_bool()));
        } else if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2005, self.get_data_type(), other.get_data_type());
        } else if self.get_data_type() == DataType::Float {
            error!(2005);
        }
        let result = self.as_num() | other.as_num();
        Box::new(Num(result, true))
    }
    /// Performs a bitwise xor with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform xor with
    ///
    /// # Returns
    /// A boxed version of the result
    fn xor(&self, other: &Box<Data>) -> Box<Data> {
        if self.get_data_type() == DataType::Bool && other.get_data_type() == DataType::Bool {
            return Box::new(Bool::from(self.as_bool() ^ other.as_bool()));
        } else if !self.supports_advanced_math() || !other.supports_advanced_math() {
            error!(2006, self.get_data_type(), other.get_data_type());
        } else if self.get_data_type() == DataType::Float {
            error!(2006);
        }
        let result = self.as_num() ^ other.as_num();
        Box::new(Num(result, true))
    }
    /// Performs a bitwise not with the piece of data if applicable
    ///
    /// # Returns
    /// A boxed version of the result
    fn bit_not(&self) -> Box<Data> {
        if self.get_data_type() == DataType::Bool {
            return Box::new(Bool::from(!self.as_bool()));
        } else if !self.supports_advanced_math() {
            error!(2007, self.get_data_type());
        }
        let byte = self.as_num();
        if byte < 0 || byte > 255 {
            error!(2008, self);
        }
        let result = !(byte as u8);
        Box::new(Num(result as i64, true))
    }
    /// Performs a power with the piece of data if applicable
    ///
    /// # Arguments
    /// * `other` - the piece of data to perform power with
    ///
    /// # Returns
    /// A boxed version of the result
    fn pow(&self, other: &Box<Data>) -> Box<Data> {
        if other.get_data_type() == DataType::Float {
            error!(2009);
        }
        if self.get_data_type() == DataType::Error || other.get_data_type() == DataType::Error {
            no_math!();
        }
        let result = self.as_num().pow(other.as_num() as u32);
        Box::new(Num(result, true))
    }

    /// Converts the data to a number
    ///
    /// # Returns
    /// The piece of data in numerical representation
    fn as_num(&self) -> i64 {
        error!(2010, self.get_data_type())
    }
    /// Converts the data to a floating point number
    ///
    /// # Returns
    /// The piece of data in floating point representation
    fn as_float(&self) -> f64 {
        self.as_num() as f64
    }
    /// Converts the data to a character
    ///
    /// # Returns
    /// The piece of data in character representation
    fn as_chr(&self) -> char {
        error!(2011, self.get_data_type())
    }
    /// Converts the data to a namespace
    ///
    /// # Returns
    /// The piece of data as a namespace
    fn as_ns(&self) -> &Vec<u64> {
        error!(2012, self.get_data_type())
    }
    /// Converts the data to a boolean
    ///
    /// # Returns
    /// The piece of data in boolean representation
    fn as_bool(&self) -> bool {
        false
    }
    /// Converts the data to a string
    ///
    /// # Returns
    /// The piece of data as a string
    fn as_str(&self) -> &String {
        error!(2014, self.get_data_type())
    }
    /// Indicates whether the data type supports advanced operations like multiplications
    ///
    /// # Returns
    /// True if such operations are supported, false otherwise
    fn supports_advanced_math(&self) -> bool {
        false
    }
    /// Compares the data to some other piece of data
    ///
    /// # Arguments
    /// * `other` - the piece of data to compare to
    ///
    /// # Returns
    /// Ordering of the data
    fn cmp(&self, other: &Box<Data>) -> Ordering;
    /// Negates the data
    ///
    /// # Returns
    /// A boxed version of the result
    fn not(&self) -> Box<Data> {
        Box::new(Bool::from(!self.as_bool()))
    }

    /// Gets the data type
    ///
    /// # Returns
    /// The type of the data
    fn get_data_type(&self) -> DataType;
    /// Gets the data type used for comparing
    ///
    /// # Returns
    /// The type used to compare to other pieces of data
    fn cmp_data_type(&self) -> DataType {
        self.get_data_type()
    }
    /// Indicates whether the piece of data can be used as a function name
    ///
    /// # Returns
    /// True if it can be used, false otherwise
    fn can_be_func_name(&self) -> bool {
        match self.get_data_type() {
            DataType::Null | DataType::Namespace | DataType::Float | DataType::Datatype | DataType::Error => false,
            _ => true,
        }
    }
}

/// Wrapper for data to make them clonable inside of boxes
pub trait DataClone {
    /// Clones a box containing a piece of data
    fn clone_box(&self) -> Box<Data>;
}

impl<T> DataClone for T where T: 'static + Data + Clone {
    fn clone_box(&self) -> Box<Data> {
        Box::new(self.clone())
    }
}

impl Clone for Box<Data> {
    fn clone(&self) -> Box<Data> {
        self.clone_box()
    }
}

/// Null value
#[derive(Clone)]
pub struct Null;

impl Data for Null {
    fn add_data(&mut self, _data: u64) -> bool {
        error!(2021)
    }

    fn is_complete(&self) -> bool {
        true
    }
    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Null {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2022)
        }
    }
    fn not(&self) -> Box<Data> {
        error!(2023)
    }
    fn get_data_type(&self) -> DataType {
        DataType::Null
    }
}

impl fmt::Display for Null {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "NULL")
    }
}

/// Any integer
#[derive(Clone)]
pub struct Num(i64, bool);

impl Data for Num {
    fn add_data(&mut self, data: u64) -> bool {
        self.0 = data as i64;
        self.1 = true;
        true
    }

    fn is_complete(&self) -> bool {
        self.1
    }

    fn as_num(&self) -> i64 {
        self.0
    }
    fn as_bool(&self) -> bool {
        self.0 != 0
    }
    fn as_chr(&self) -> char {
        if self.0 < 0 || self.0 > 255 {
            error!(2013, self);
        }
        self.0 as u8 as char
    }
    fn supports_advanced_math(&self) -> bool {
        true
    }
    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Num || other.get_data_type() == DataType::Error {
            self.0.cmp(&other.as_num())
        } else if other.get_data_type() == DataType::Float {
            if other.as_float() - self.as_float() > 0.01f64 {
                Ordering::Less
            } else if self.as_float() - other.as_float() > 0.01f64 {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Num {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2031, other.get_data_type())
        }
    }
    fn get_data_type(&self) -> DataType {
        DataType::Num
    }
}

impl fmt::Display for Num {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.1 {
            write!(f, "{}", self.0)
        } else {
            write!(f, "Unset")
        }
    }
}

impl Num {
    /// Creates a new number
    ///
    /// # Returns
    /// The newly created number
    pub fn new() -> Self {
        Num(0, false)
    }
    /// Creates a number from a given integer
    ///
    /// # Arguments
    /// * `num` - the integer to use as a reference
    ///
    /// # Returns
    /// The newly created number
    pub fn from(num: i64) -> Self {
        Num(num, true)
    }

    /// Version of add_data for negative numbers
    ///
    /// # Arguments
    /// * `data` - the number to add
    ///
    /// # Returns
    /// `true` since the data is complete after adding a number
    pub fn add_data_neg(&mut self, data: i64) -> bool {
        self.0 = data;
        self.1 = true;
        true
    }
}

/// Single character
#[derive(Clone)]
pub struct Char(char, bool);

impl Data for Char {
    fn add_data(&mut self, data: u64) -> bool {
        self.0 = data as u8 as char;
        self.1 = true;
        true
    }

    fn is_complete(&self) -> bool {
        self.1
    }

    fn add(&self, other: &Box<Data>) -> Box<Data> {
        let result = (self.0 as i16 + other.as_num() as i16) as u8 as char;
        Box::new(Char(result, true))
    }
    fn sub(&self, other: &Box<Data>) -> Box<Data> {
        let result = (self.0 as i16 - other.as_num() as i16) as u8 as char;
        Box::new(Char(result, true))
    }

    fn as_chr(&self) -> char {
        self.0
    }
    fn as_num(&self) -> i64 {
        self.0 as i64
    }
    fn as_bool(&self) -> bool {
        true
    }
    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Char {
            self.0.cmp(&other.as_chr())
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Char {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2032, other.get_data_type())
        }
    }
    fn get_data_type(&self) -> DataType {
        DataType::Char
    }
}

impl fmt::Display for Char {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Char {
    /// Creates a new Char
    ///
    /// # Returns
    /// The newly created data
    pub fn new() -> Self {
        Char(0u8 as char, false)
    }
    /// Creates a new Char from a reference value
    ///
    /// # Arguments
    /// * `chr` - the reference value
    ///
    /// # Returns
    /// The newly created data
    pub fn from(chr: char) -> Self {
        Char(chr, true)
    }

    /// Version of add_data accepting a character
    ///
    /// # Arguments
    /// * `data` - the character to set the data to
    ///
    /// # Returns
    /// `true` since the data is complete after adding a character
    pub fn add_data_char(&mut self, data: char) -> bool {
        self.0 = data;
        self.1 = true;
        true
    }
}

/// Boolean
#[derive(Clone)]
pub struct Bool(bool, bool);

impl Data for Bool {
    fn add_data(&mut self, data: u64) -> bool {
        self.0 = data != 0;
        self.1 = true;
        true
    }

    fn is_complete(&self) -> bool {
        self.1
    }

    fn as_num(&self) -> i64 {
        self.0 as i64
    }
    fn as_bool(&self) -> bool {
        self.0
    }
    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Bool {
            self.0.cmp(&other.as_bool())
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Bool {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2033, other.get_data_type())
        }
    }
    fn get_data_type(&self) -> DataType {
        DataType::Bool
    }
}

impl fmt::Display for Bool {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.1 {
            write!(f, "{}", self.0)
        } else {
            write!(f, "Unset")
        }
    }
}

impl Bool {
    /// Creates a new Bool
    ///
    /// # Returns
    /// The newly created data
    pub fn new() -> Self {
        Bool(false, false)
    }

    /// Creates a new Bool from a reference value
    ///
    /// # Arguments
    /// * `value` - the reference value
    ///
    /// # Returns
    /// The newly created data
    pub fn from(value: bool) -> Self {
        Bool(value, true)
    }

    /// Version of add_data accepting a boolean
    ///
    /// # Arguments
    /// * `data` - the boolean to set the data to
    ///
    /// # Returns
    /// `true` since the data is complete after adding a boolean
    pub fn add_data_bool(&mut self, data: bool) -> bool {
        self.0 = data;
        self.1 = true;
        true
    }
}

/// Function namespace
#[derive(Clone)]
pub struct Namespace(Vec<u64>, u64);

impl Data for Namespace {
    fn add_data(&mut self, data: u64) -> bool {
        if self.1 == 0 {
            self.1 = data;
        } else {
            self.0.push(data);
        }
        self.is_complete()
    }

    fn is_complete(&self) -> bool {
        self.1 > 0 && self.0.len() == self.1 as usize
    }

    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Namespace {
            self.0.cmp(other.as_ns())
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Namespace {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2034, other.get_data_type())
        }
    }

    fn get_data_type(&self) -> DataType {
        DataType::Namespace
    }

    fn as_ns(&self) -> &Vec<u64> {
        &self.0
    }
}

impl fmt::Display for Namespace {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.1 > 0 {
            if self.is_complete() {
                write!(f, "NS {:?}", self.0.iter().map(|part| num_as_q!(*part)).collect::<Vec<_>>())
            } else {
                write!(f, "Incomplete")
            }
        } else {
            write!(f, "Unset")
        }
    }
}

impl Namespace {
    /// Creates a new empty namespace
    ///
    /// # Returns
    /// The newly created namespace
    pub fn new() -> Self {
        Namespace(vec![], 0)
    }
    /// Creates a new namespace from a reference value
    ///
    /// # Arguments
    /// * `ns` - the reference value
    ///
    /// # Returns
    /// The newly created namespace
    pub fn from(ns: Vec<u64>) -> Self {
        let len = ns.len() as u64;
        Namespace(ns, len)
    }
}

/// Floating point number
#[derive(Clone)]
pub struct Float(f64, bool);

impl Data for Float {
    fn add_data(&mut self, data: u64) -> bool {
        self.0 = data as f64;
        self.1 = true;
        true
    }

    fn is_complete(&self) -> bool {
        self.1
    }

    fn and(&self, _other: &Box<Data>) -> Box<Data> {
        error!(2004)
    }

    fn or(&self, _other: &Box<Data>) -> Box<Data> {
        error!(2005)
    }

    fn xor(&self, _other: &Box<Data>) -> Box<Data> {
        error!(2006)
    }

    fn bit_not(&self) -> Box<Data> {
        error!(2007)
    }

    fn pow(&self, _other: &Box<Data>) -> Box<Data> {
        error!(2009)
    }

    fn as_num(&self) -> i64 {
        self.0 as i64
    }

    fn as_float(&self) -> f64 {
        self.0
    }

    fn supports_advanced_math(&self) -> bool {
        true
    }

    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Num || other.get_data_type() == DataType::Float {
            if other.as_float() - self.as_float() > 0.01f64 {
                Ordering::Less
            } else if self.as_float() - other.as_float() > 0.01f64 {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Float {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2035, other.get_data_type())
        }
    }

    fn get_data_type(&self) -> DataType {
        DataType::Float
    }
}

impl fmt::Display for Float {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.1 {
            if self.0 % 1.0 == 0.0 {
                write!(f, "{:.1}", self.0)
            } else {
                write!(f, "{}", self.0)
            }
        } else {
            write!(f, "Unset")
        }
    }
}

impl Float {
    /// Creates a new Float
    ///
    /// # Returns
    /// The newly created data
    pub fn new() -> Self {
        Float(0.0f64, false)
    }
    /// Creates a new Float from a reference value
    ///
    /// # Arguments
    /// * `num` - the reference value
    ///
    /// # Returns
    /// The newly created data
    pub fn from(num: f64) -> Self {
        Float(num, true)
    }

    /// Version of add_data accepting a float
    ///
    /// # Arguments
    /// * `data` - the float to set the data to
    ///
    /// # Returns
    /// `true` since the data is complete after adding a float
    pub fn add_data_float(&mut self, data: f64) -> bool {
        self.0 = data;
        self.1 = true;
        true
    }
}

/// Data type
#[derive(Clone)]
pub struct Dtype {
    cmp_type: DataType,
}

impl Data for Dtype {
    fn add_data(&mut self, data: u64) -> bool {
        self.cmp_type = match data {
            1 => DataType::Null,
            2 => DataType::Num,
            3 => DataType::Char,
            4 => DataType::Bool,
            5 => DataType::Float,
            6 => DataType::Namespace,
            7 => DataType::Error,
            x => error!(10201, num_as_q!(x)),
        };
        true
    }

    fn is_complete(&self) -> bool {
        self.cmp_type != DataType::Datatype
    }

    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == self.cmp_type {
            Ordering::Equal
        } else {
            Ordering::Less
        }
    }

    fn get_data_type(&self) -> DataType {
        DataType::Datatype
    }

    fn cmp_data_type(&self) -> DataType {
        self.cmp_type.clone()
    }
}

impl fmt::Display for Dtype {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "type({:?})", self.cmp_type)
    }
}

impl Dtype {
    /// Creates a new generic data type
    ///
    /// # Returns
    /// The newly created data
    pub fn new() -> Self {
        Dtype { cmp_type: DataType::Datatype }
    }
    /// Creates a new data type from a reference value
    ///
    /// # Arguments
    /// * `cmp_type` - the reference value
    ///
    /// # Returns
    /// The newly created data
    pub fn from(cmp_type: DataType) -> Self {
        Dtype { cmp_type }
    }
}

/// Error consisting of error code and message
#[derive(Clone)]
pub struct Error {
    code: u64,
    message: String,
}

impl Data for Error {
    fn add_data(&mut self, _data: u64) -> bool {
        error!(2024)
    }

    fn is_complete(&self) -> bool {
        true
    }

    fn cmp(&self, other: &Box<Data>) -> Ordering {
        if other.get_data_type() == DataType::Num || other.get_data_type() == DataType::Error {
            self.code.cmp(&(other.as_num() as u64))
        } else if other.get_data_type() == DataType::Datatype {
            if other.cmp_data_type() == DataType::Error {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        } else {
            error!(2036, other.get_data_type())
        }
    }

    fn get_data_type(&self) -> DataType {
        DataType::Error
    }

    fn as_num(&self) -> i64 {
        self.code as i64
    }
    fn as_str(&self) -> &String {
        &self.message
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error {}: {}", self.code, self.message)
    }
}

impl Error {
    /// Creates a new error
    ///
    /// # Returns
    /// The newly created data
    pub fn new() -> Self {
        Error::from(0u64, "An unknown error occurred".to_string())
    }
    /// Creates a new error for a code and message
    ///
    /// # Arguments
    /// * `code` - the error code
    /// * `message` - the error message
    ///
    /// # Returns
    /// The newly created data
    pub fn from(code: u64, message: String) -> Self {
        Error { code, message }
    }
}
