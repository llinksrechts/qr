/// Program stack
pub mod stack;
/// Program heap
pub mod heap;
/// Data and data types
pub mod data;