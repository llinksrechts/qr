use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::{Null, Num};
use std::process::{Command, Stdio};

/// Function to execute a shell command
#[derive(Clone)]
pub struct BashFunction;

impl Function for BashFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let arg_num = stack.pop_one().as_num();
        let mut args = vec![];
        for _ in 0..(arg_num - 1) {
            args.push(fmt_data_vec!(stack.pop_str()));
        }
        args.reverse();
        Command::new(fmt_data_vec!(stack.pop_str())).args(args.into_iter()).stdout(Stdio::piped()).spawn().unwrap();
    }
}

/// Function to execute a shell command and capture its output
#[derive(Clone)]
pub struct BashOutFunction;

impl Function for BashOutFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let arg_num = stack.pop_one().as_num();
        let mut args = vec![];
        for _ in 0..(arg_num - 1) {
            args.push(fmt_data_vec!(stack.pop_str()));
        }
        args.reverse();
        let output = Command::new(fmt_data_vec!(stack.pop_str())).args(args.into_iter()).output().unwrap();
        stack.push(Box::new(Null));
        stack.push_multiple(str_to_data_vec!(output.stdout));
        stack.push(Box::new(Num::from(output.status.code().unwrap() as i64)));
    }
}
