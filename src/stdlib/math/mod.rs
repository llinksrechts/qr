use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::{Num, Float, DataType, Data};
use util::mod_pow;

/// Trigonometric functions
pub mod trig;

/// Function to calculate powers
#[derive(Clone)]
pub struct PowFunction;

impl Function for PowFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let power = stack.pop_one().as_float();
        let base = stack.pop_one().as_float();
        let result = Float::from(base.powf(power));
        #[cfg(debug_assertions)]
            println!("Pow {} ^ {} = {}", base, power, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate powers modulo some number
#[derive(Clone)]
pub struct ModPowFunction;

impl Function for ModPowFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let rem = stack.pop_one().as_num();
        let power = stack.pop_one().as_num();
        let base = stack.pop_one().as_num();
        let result = Num::from(mod_pow(base, power, rem));
        #[cfg(debug_assertions)]
            println!("Pow {} ^ {} = {} mod {}", base, power, result, rem);
        stack.push(Box::new(result));
    }
}

/// Function to calculate square roots
#[derive(Clone)]
pub struct SqrtFunction;

impl Function for SqrtFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.sqrt());
        #[cfg(debug_assertions)]
            println!("Sqrt({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to round a number
#[derive(Clone)]
pub struct RoundFunction;

impl Function for RoundFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Num::from(number.round() as i64);
        #[cfg(debug_assertions)]
            println!("Round {} ~ {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to round a number up
#[derive(Clone)]
pub struct CeilFunction;

impl Function for CeilFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Num::from(number.ceil() as i64);
        #[cfg(debug_assertions)]
            println!("Ceil {} ~ {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to round a number down
#[derive(Clone)]
pub struct FloorFunction;

impl Function for FloorFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Num::from(number.floor() as i64);
        #[cfg(debug_assertions)]
            println!("Floor {} ~ {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the natural logarithm
#[derive(Clone)]
pub struct LnFunction;

impl Function for LnFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.ln());
        #[cfg(debug_assertions)]
            println!("ln({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the decadic logarithm
#[derive(Clone)]
pub struct LogFunction;

impl Function for LogFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.log10());
        #[cfg(debug_assertions)]
            println!("log({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the absolute value of a number
#[derive(Clone)]
pub struct AbsFunction;

impl Function for AbsFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one();
        let result: Box<Data> = match number.get_data_type() {
            DataType::Float => Box::new(Float::from(number.as_float().abs())),
            _ => Box::new(Num::from(number.as_num().abs())),
        };
        #[cfg(debug_assertions)]
            println!("abs({}) = {}", number, result);
        stack.push(result);
    }
}
