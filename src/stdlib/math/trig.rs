use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::Float;

/// Function to calculate the sine
#[derive(Clone)]
pub struct SinFunction;

impl Function for SinFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.sin());
        #[cfg(debug_assertions)]
            println!("sin({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the cosine
#[derive(Clone)]
pub struct CosFunction;

impl Function for CosFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.cos());
        #[cfg(debug_assertions)]
            println!("cos({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the tangent
#[derive(Clone)]
pub struct TanFunction;

impl Function for TanFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.tan());
        #[cfg(debug_assertions)]
            println!("tan({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse sine
#[derive(Clone)]
pub struct AsinFunction;

impl Function for AsinFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.asin());
        #[cfg(debug_assertions)]
            println!("asin({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse cosine
#[derive(Clone)]
pub struct AcosFunction;

impl Function for AcosFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.acos());
        #[cfg(debug_assertions)]
            println!("acos({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse tangent
#[derive(Clone)]
pub struct AtanFunction;

impl Function for AtanFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.atan());
        #[cfg(debug_assertions)]
            println!("atan({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the hyperbolic sine
#[derive(Clone)]
pub struct SinhFunction;

impl Function for SinhFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.sinh());
        #[cfg(debug_assertions)]
            println!("sinh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the hyperbolic cosine
#[derive(Clone)]
pub struct CoshFunction;

impl Function for CoshFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.cosh());
        #[cfg(debug_assertions)]
            println!("cosh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the hyperbolic tangent
#[derive(Clone)]
pub struct TanhFunction;

impl Function for TanhFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.tanh());
        #[cfg(debug_assertions)]
            println!("tanh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse hyperbolic sine
#[derive(Clone)]
pub struct AsinhFunction;

impl Function for AsinhFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.asinh());
        #[cfg(debug_assertions)]
            println!("asinh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse hyperbolic cosine
#[derive(Clone)]
pub struct AcoshFunction;

impl Function for AcoshFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.acosh());
        #[cfg(debug_assertions)]
            println!("acosh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}

/// Function to calculate the inverse hyperbolic tangent
#[derive(Clone)]
pub struct AtanhFunction;

impl Function for AtanhFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let number = stack.pop_one().as_float();
        let result = Float::from(number.atanh());
        #[cfg(debug_assertions)]
            println!("atanh({}) = {}", number, result);
        stack.push(Box::new(result));
    }
}
