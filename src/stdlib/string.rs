use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::{Data, Null, Num, Char};

/// Function to determine the length of a string
#[derive(Clone)]
pub struct LenFunction;

impl Function for LenFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let mut len = 1u64;
        loop {
            if stack.get(len).is_null() {
                break;
            }
            len += 1;
        }
        stack.push(Box::new(Num::from((len-1) as i64)));
    }
}

/// Function to count matches of a string in another string
#[derive(Clone)]
pub struct CountFunction;

impl Function for CountFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let needle = fmt_data_vec!(stack.pop_str());
        let haystack = fmt_data_vec!(stack.get_str());
        stack.push(Box::new(Num::from(haystack.matches(&needle).count() as i64)));
    }
}

/// Function to replace a string in another string
#[derive(Clone)]
pub struct ReplaceFunction;

impl Function for ReplaceFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let to = fmt_data_vec!(stack.pop_str());
        let from = fmt_data_vec!(stack.pop_str());
        let source = fmt_data_vec!(stack.pop_str());

        let mut result = vec![Box::new(Null) as Box<Data>];
        result.extend(str_to_data_vec!(source.replace(&from, &to).chars()));
        stack.push_multiple(result);
    }
}

/// Function to split a string at a delimiter
#[derive(Clone)]
pub struct SplitFunction;

impl Function for SplitFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let delimiter = fmt_data_vec!(stack.pop_str());
        let source = fmt_data_vec!(stack.pop_str());

        let result = source.split(&delimiter).collect::<Vec<_>>().into_iter().rev().collect::<Vec<_>>();
        let len = result.len() as i64;
        for part in result {
            let mut data = vec![Box::new(Null) as Box<Data>];
            data.extend(str_to_data_vec!(part.chars()));
            stack.push_multiple(data);
        }
        stack.push(Box::new(Num::from(len)));
    }
}

/// Function to join strings with a delimiter
#[derive(Clone)]
pub struct JoinFunction;

impl Function for JoinFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let delimiter = fmt_data_vec!(stack.pop_str());
        let count = stack.pop_one().as_num();
        let mut parts = vec![];
        for _ in 0..count {
            parts.push(fmt_data_vec!(stack.pop_str()));
        }
        let result = parts.join(&delimiter);
        let mut data = vec![Box::new(Null) as Box<Data>];
        data.extend(str_to_data_vec!(result.chars()));
        stack.push_multiple(data);
    }
}

/// Function to convert a string to lower case
#[derive(Clone)]
pub struct LowerFunction;

impl Function for LowerFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let source = fmt_data_vec!(stack.pop_str());
        let result = source.to_lowercase();
        let mut data = vec![Box::new(Null) as Box<Data>];
        data.extend(str_to_data_vec!(result.chars()));
        stack.push_multiple(data);
    }
}

/// Function to convert a string to upper case
#[derive(Clone)]
pub struct UpperFunction;

impl Function for UpperFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let source = fmt_data_vec!(stack.pop_str());
        let result = source.to_uppercase();
        let mut data = vec![Box::new(Null) as Box<Data>];
        data.extend(str_to_data_vec!(result.chars()));
        stack.push_multiple(data);
    }
}

/// Function to trim a string
#[derive(Clone)]
pub struct TrimFunction;

impl Function for TrimFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let source = fmt_data_vec!(stack.pop_str());
        let result = source.trim();
        let mut data = vec![Box::new(Null) as Box<Data>];
        data.extend(str_to_data_vec!(result.chars()));
        stack.push_multiple(data);
    }
}

/// Function to create a string from a byte array
#[derive(Clone)]
pub struct FromByteArrayFunction;

impl Function for FromByteArrayFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let length = stack.pop_one().as_num();
        let mut data: Vec<Box<Data>> = vec![];
        for _ in 0..length {
            data.push(Box::new(Char::from(stack.pop_one().as_chr())));
        }
        data.push(Box::new(Null));
        data.reverse();
        stack.push_multiple(data);
    }
}

/// Function to convert a string to a byte array
#[derive(Clone)]
pub struct ToByteArrayFunction;

impl Function for ToByteArrayFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let data = stack.pop_str().into_iter().map(|c| Box::new(Num::from(c.as_num())) as Box<Data>).rev().collect::<Vec<_>>();
        let len = data.len() as i64;
        stack.push_multiple(data);
        stack.push(Box::new(Num::from(len)));
    }
}
