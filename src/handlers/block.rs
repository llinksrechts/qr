use handlers::{Handler, RootHandler};
use commands::Command;
use commands::block::{IfCommand, WhileCommand, FuncCommand, AnonFuncCommand};
use commands::block::control::{BreakCommand, ContCommand, ReturnCommand};
use commands::block::call::CallCommand;

/// Type of block
#[derive(Clone, PartialEq)]
enum BlockType {
    /// If block
    If,
    /// Loop
    While,
    /// Named function
    NamedFunc,
    /// Anonymous function
    AnonFunc,
    /// Block type not yet known
    Unknown,
}

/// Handler for any type of block
#[derive(Clone)]
pub struct BlockHandler {
    block_type: BlockType,
    child_handlers: Vec<Box<Handler>>,
    is_breaking: bool,
    complete: bool,
}

impl Handler for BlockHandler {
    fn generate_command(self: Box<Self>) -> Box<Command> {
        match self.block_type {
            BlockType::If => {
                let child_commands = self.child_handlers.into_iter().map(|h| h.generate_command()).collect::<Vec<_>>();
                Box::new(IfCommand::new(child_commands))
            }
            BlockType::While => {
                let child_commands = self.child_handlers.into_iter().map(|h| h.generate_command()).collect::<Vec<_>>();
                Box::new(WhileCommand::new(child_commands))
            }
            BlockType::NamedFunc => {
                let child_commands = self.child_handlers.into_iter().map(|h| h.generate_command()).collect::<Vec<_>>();
                Box::new(FuncCommand::new(child_commands))
            }
            BlockType::AnonFunc => {
                let child_commands = self.child_handlers.into_iter().map(|h| h.generate_command()).collect::<Vec<_>>();
                Box::new(AnonFuncCommand::new(child_commands))
            }
            _ => unimplemented!(),
        }
    }
    fn all_complete(&mut self) -> bool {
        self.complete
    }
    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.block_type == BlockType::Unknown {
            self.block_type = match arg {
                1 => BlockType::If,
                2 => BlockType::While,
                3 => BlockType::NamedFunc,
                4 => BlockType::AnonFunc,
                _ => error!(9022, num_as_q!(arg)),
            }
        } else {
            if self.child_handlers.len() == 0 {
                self.append_handler(arg);
            } else {
                let last_index = self.child_handlers.len() - 1;
                if self.child_handlers[last_index].all_complete() {
                    self.append_handler(arg);
                } else {
                    self.child_handlers[last_index].add_argument(arg);
                }
            }
        }
        self.complete
    }
}

impl BlockHandler {
    /// Creates a new BlockHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        BlockHandler { block_type: BlockType::Unknown, child_handlers: vec![], complete: false, is_breaking: false }
    }
    /// Appends a child to the block
    ///
    /// # Arguments
    /// * `arg` - The interpreter token to use
    fn append_handler(&mut self, arg: u64) {
        if self.is_breaking {
            match arg {
                1 => self.complete = true,
                2 => self.child_handlers.push(Box::new(ContHandler::new())),
                3 => self.child_handlers.push(Box::new(BreakHandler::new())),
                4 => self.child_handlers.push(Box::new(ReturnHandler)),
                _ => error!(9021, num_as_q!(arg)),
            }
            self.is_breaking = false;
        } else {
            if arg == 6 {
                self.is_breaking = true;
            } else {
                self.child_handlers.push(RootHandler::get_handler_for_id(arg));
            }
        }
    }
}

/// Handler for the `continue` statement
#[derive(Clone)]
struct ContHandler(u64);

impl Handler for ContHandler {
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn is_complete(&self) -> bool {
        self.0 > 0
    }
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.0 = arg;
        true
    }
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(ContCommand::new(self.0))
    }
}

impl ContHandler {
    /// Creates a new ContHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        ContHandler(0)
    }
}

/// Handler for the `break` statement
#[derive(Clone)]
struct BreakHandler(u64);

impl Handler for BreakHandler {
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn is_complete(&self) -> bool {
        self.0 > 0
    }
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.0 = arg;
        true
    }
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(BreakCommand::new(self.0))
    }
}

impl BreakHandler {
    /// Creates a new BreakHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        BreakHandler(0)
    }
}

/// Handler for the `return` statement
#[derive(Clone)]
struct ReturnHandler;

impl Handler for ReturnHandler {
    fn all_complete(&mut self) -> bool {
        true
    }
    fn is_complete(&self) -> bool {
        true
    }
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(ReturnCommand)
    }
}

/// Handler for function calls
#[derive(Clone)]
pub struct CallHandler;

impl Handler for CallHandler {
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(CallCommand)
    }
    fn is_complete(&self) -> bool { true }
    fn all_complete(&mut self) -> bool { true }
}
