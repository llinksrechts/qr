use handlers::Handler;
use handlers::data::DataHandler;
use commands::Command;
use commands::stack::{PushCommand, PopCommand, GetCommand, GetStrCommand, MoveCommand, MoveStrCommand};

/// Handler for stack operations
#[derive(Clone)]
pub struct StackHandler {
    next_handler: Option<Box<Handler>>,
}

impl Handler for StackHandler {
    fn next_handler(&mut self) -> &mut Box<Handler> {
        self.next_handler.as_mut().unwrap()
    }
    fn has_next_handler(&self) -> bool { self.next_handler.is_some() }
    fn can_have_next_handler(&self) -> bool { true }
    fn get_next_handler(&self, num: u64) -> Box<Handler> {
        match num {
            1 => Box::new(PushHandler::new()),
            2 => Box::new(PopHandler::new()),
            3 => Box::new(GetHandler),
            4 => Box::new(GetStrHandler),
            5 => Box::new(MoveHandler),
            6 => Box::new(MoveStrHandler),
            _ => error!(10121, num_as_q!(num)),
        }
    }
    fn populate_next_handler(&mut self, handler: Box<Handler>) {
        self.next_handler = Some(handler);
    }
    fn is_complete(&self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        self.next_handler.unwrap().generate_command()
    }
}

impl StackHandler {
    /// Creates a new StackHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        StackHandler { next_handler: None }
    }
}

/// Handler for the push operation
#[derive(Clone)]
pub struct PushHandler {
    arg_handler: Box<Handler>,
}

impl Handler for PushHandler {
    fn next_handler(&mut self) -> &mut Box<Handler> {
        &mut self.arg_handler
    }
    fn has_next_handler(&self) -> bool { true }
    fn can_have_next_handler(&self) -> bool { true }
    fn is_complete(&self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(PushCommand::new(self.arg_handler.get_data()))
    }
}

impl PushHandler {
    /// Creates a new PushHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        PushHandler { arg_handler: Box::new(DataHandler::new()) }
    }
}

/// Handler for the pop operation
#[derive(Clone)]
pub struct PopHandler {
    action: u8,
    amount: Option<u64>,
}

impl Handler for PopHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.action == 0u8 {
            if arg < 1 || arg > 4 {
                error!(1012, arg);
            }
            self.action = arg as u8;
            arg != 2
        } else {
            if self.action != 2 {
                error!(91012);
            }
            self.amount = Some(arg);
            true
        }
    }
    fn is_complete(&self) -> bool {
        self.action > 0u8 && (self.action != 2u8 || self.amount.is_some())
    }
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(PopCommand::new(self.action, self.amount))
    }
}

impl PopHandler {
    /// Creates a new PopHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        PopHandler { action: 0u8, amount: None }
    }
}

/// Handler for the get operation
#[derive(Clone)]
pub struct GetHandler;

impl Handler for GetHandler {
    fn is_complete(&self) -> bool { true }

    fn all_complete(&mut self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(GetCommand)
    }
}

/// Handler for the get string operation
#[derive(Clone)]
pub struct GetStrHandler;

impl Handler for GetStrHandler {
    fn is_complete(&self) -> bool { true }

    fn all_complete(&mut self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(GetStrCommand)
    }
}

/// Handler for the move operation
#[derive(Clone)]
pub struct MoveHandler;

impl Handler for MoveHandler {
    fn is_complete(&self) -> bool { true }

    fn all_complete(&mut self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(MoveCommand)
    }
}

/// Handler for the move string operation
#[derive(Clone)]
pub struct MoveStrHandler;

impl Handler for MoveStrHandler {
    fn is_complete(&self) -> bool { true }

    fn all_complete(&mut self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(MoveStrCommand)
    }
}
