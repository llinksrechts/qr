use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::Heap;

/// Command handling a `continue`-statement
#[derive(Clone)]
pub struct ContCommand(u64);

impl Command for ContCommand {
    fn execute(&self, _stack: &mut Stack, _heap: &mut Heap) -> Escape {
        Escape::Continue(self.0)
    }
}

impl ContCommand {
    /// Creates a new ContCommand
    ///
    /// # Arguments
    ///
    /// * `amount` - How many loops to continue (breaks `amount - 1` loops)
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(amount: u64) -> Self {
        ContCommand(amount)
    }
}

/// Command handling a `break`-statement
#[derive(Clone)]
pub struct BreakCommand(u64);

impl Command for BreakCommand {
    fn execute(&self, _stack: &mut Stack, _heap: &mut Heap) -> Escape {
        Escape::Break(self.0)
    }
}

impl BreakCommand {
    /// Creates a new BreakCommand
    ///
    /// # Arguments
    ///
    /// * `amount` - How many loops to break out of
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(amount: u64) -> Self {
        BreakCommand(amount)
    }
}

/// Command handling a `return`-statement
#[derive(Clone)]
pub struct ReturnCommand;

impl Command for ReturnCommand {
    fn execute(&self, _stack: &mut Stack, _heap: &mut Heap) -> Escape {
        Escape::Return
    }
}

