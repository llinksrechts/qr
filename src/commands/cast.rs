use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::Heap;
use memory::data::{Num, Char, Bool, Float};

/// Command handling a cast
#[derive(Clone)]
pub struct CastCommand {
    target_type: u64,
}

impl Command for CastCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        match self.target_type {
            2 => {
                #[cfg(debug_assertions)]
                    println!("Casting to number");
                let old_value = stack.pop_one();
                stack.push(Box::new(Num::from(old_value.as_num())));
            }
            3 => {
                #[cfg(debug_assertions)]
                    println!("Casting to char");
                let old_value = stack.pop_one();
                stack.push(Box::new(Char::from(old_value.as_chr())));
            }
            4 => {
                #[cfg(debug_assertions)]
                    println!("Casting to bool");
                let old_value = stack.pop_one();
                stack.push(Box::new(Bool::from(old_value.as_bool())));
            }
            5 => {
                #[cfg(debug_assertions)]
                    println!("Casting to float");
                let old_value = stack.pop_one();
                stack.push(Box::new(Float::from(old_value.as_float())));
            }
            x => {
                error!(1101, num_as_q!(x));
            },
        }
        Escape::None
    }
}

impl CastCommand {
    /// Creates a new CastCommand
    ///
    /// # Arguments
    ///
    /// * `target_type` - The type to cast to
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(target_type: u64) -> Self {
        CastCommand { target_type }
    }
}
